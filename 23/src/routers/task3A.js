import express from 'express';
import fetch from 'isomorphic-fetch';


// DB
const pc80286URL = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

let pc80286 = {};
fetch(pc80286URL).then(async (res) => {
  pc80286 = await res.json();
}).catch(err => {
  console.log(err);
});


// APP
let router = express.Router();

router.get("/volumes", async (req, res) => {
  let len = pc80286.hdd.length;
  let hdd = {};
  for (let i = 0; i < len; i++) {
    let volume = pc80286.hdd[i].volume;
    if (!hdd.hasOwnProperty(volume)) {
      hdd[volume] = 0;
    }
    hdd[volume] += pc80286.hdd[i].size;
  }

  Object.keys(hdd).forEach(key => {
    var val = hdd[key];
    hdd[key] = hdd[key] + "B";
  });

  res.json(hdd);
});

router.get("/*", async (req, res) => {
  let result = pc80286;
  let error = false;

  let path = req.url.replace(/\//g, " ").trim();

  if (path.length > 0) {
    let items = path.split(" ");

    while (items.length > 0 && !error) {
      let item = items.shift();
      if (result.hasOwnProperty(item) && result.propertyIsEnumerable(item)) {
        result = result[item];
      } else {
        error = true;
      }
    }
  }

  if (!error) {
    res.json(result);
  } else {
    res.status(404).send("Not Found");
  }
});


// EXPORT
module.exports = router;

