var http = require('http');
var url = require('url');

http.createServer(function(request, response) {
	var parts = url.parse(request.url, true);
	var query = parts.query;
	
	var a = parseInt(parts.query.a);
	var b = parseInt(parts.query.b);
	
	var res = 0;
	if (!isNaN(a)) {
		res += a;
	}
	if (!isNaN(b)) {
		res += b;
	}
	
	console.log(res);
	
	response.writeHead(200, {'Content-Type':'text/plain'});
	response.write(res.toString());
	response.end();
}).listen(8888);