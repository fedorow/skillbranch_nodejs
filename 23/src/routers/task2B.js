import express from 'express';


// APP
let router = express.Router();

router.get("/", async (req, res) => {
	var fullname = req.query.fullname;
	fullname = fullname.trim().toUpperCase();

	var result = "";
	var invalid = false;

	if (!fullname) {
		invalid = true;
	} else {
		var names = fullname.split(/ +/g);

		if (names.length > 3) {
			invalid = true;
		} else {
			while(names.length < 3) {
				names.unshift("");
			}

			if (/^[^0-9_\/]*$/gi.test(names[2])) {
				result = names[2].substr(0, 1) + names[2].substr(1).toLowerCase();
			} else {
				invalid = true;
			}

			if (names[0]) {
				if (/^[^0-9_\/]*$/gi.test(names[0])) {
					result += (" " + names[0].substr(0, 1) + ".");
				} else {
					invalid = true;
				}
			}

			if (names[1]) {
				if (/^[^0-9_\/]*$/gi.test(names[1])) {
					result += (" " + names[1].substr(0, 1) + ".");
				} else {
					invalid = true;
				}
			}
		}
	}

	if (invalid) {
		result = "Invalid fullname";
	}

	res.send(result);
});


// EXPORT
module.exports = router;

