import express from 'express';


// DB

let db = require('../pokemons.json');


// APP
let router = express.Router();

router.get("/(:sort)?", async (req, res) => {
  let arr = db.concat();

  arr = arr.sort((a, b) => {
    let paramA;
    let paramB;

    switch (req.params.sort) {
      case "fat": {
        paramA = parseInt(b.weight) / parseInt(b.height);
        paramB = parseInt(a.weight) / parseInt(a.height);
        break;
      }
      case "angular": {
        paramA = parseInt(a.weight) / parseInt(a.height);
        paramB = parseInt(b.weight) / parseInt(b.height);
        break;
      }
      case "heavy": {
        paramA = parseInt(b.weight);
        paramB = parseInt(a.weight);
        break;
      }
      case "light": {
        paramA = parseInt(a.weight);
        paramB = parseInt(b.weight);
        break;
      }
      case "huge": {
        paramA = parseInt(b.height);
        paramB = parseInt(a.height);
        break;
      }
      case "micro": {
        paramA = parseInt(a.height);
        paramB = parseInt(b.height);
        break;
      }
    }

    if (!paramA && !paramB || paramA == paramB) {
      paramA = a.name;
      paramB = b.name;
    }

    if (paramA > paramB) {
      return 1;
    }
    if (paramB > paramA) {
      return -1;
    }
    return 0;
  });

  let offset = req.query.offset ? parseInt(req.query.offset) : 0;
  arr = arr.slice(offset);

  let limit = req.query.limit ? parseInt(req.query.limit) : 20;
  if (limit < arr.length) {
    arr = arr.slice(0, limit);
  }

  let resArr = [];
  for (let i = 0; i < arr.length; i++) {
    resArr[i] = arr[i].name;
  }
  res.json(resArr);
});

// EXPORT
module.exports = router;
