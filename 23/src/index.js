import express from 'express';
import cors from 'cors';

import requestLogger from './middlewares/requestLogger';

import task2A from './routers/task2A';
import task2B from './routers/task2B';
import task2C from './routers/task2C';
import task2D from './routers/task2D';
import task2X from './routers/task2X';
import task3A from './routers/task3A';
import task3B from './routers/task3B';
import task3C from './routers/task3C';


const app = express();

app.use(cors());
app.use(requestLogger);

app.use("/2A", task2A);
app.use("/2B", task2B);
app.use("/2C", task2C);
app.use("/2D", task2D);
app.use("/2X", task2X);
app.use("/3A", task3A);
app.use("/3B", task3B);
app.use("/3C", task3C);

app.listen(3000, () => {
	console.log('App started');
});
