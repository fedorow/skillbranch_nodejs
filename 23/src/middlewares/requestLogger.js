import url from 'url';

var requestLogger = (req, res, next) => {
  let urlParsed = url.parse(req.url);

  console.log(req.method, urlParsed.pathname, req.query);
  next();
};

module.exports = requestLogger;
