import express from 'express';
import fetch from 'isomorphic-fetch';

// DB
const petsURL = 'https://gist.githubusercontent.com/isuvorov/55f38b82ce263836dadc0503845db4da/raw/pets.json';

let db = {};
fetch(petsURL).then(async (res) => {
  db = await res.json();
}).catch(err => {
  console.log(err);
});


// APP
let router = express.Router();

router.get("/", async (req, res) => {
  res.json(db);
});

router.get("/users(/:id)?(/:action)?", async (req, res) => {
  let havePet = req.query.havePet;

  if (req.params.id) {
    let user = findUser(req.params.id);

    if (user) {
      if (req.params.action && req.params.action == "pets") {
        res.json(findPetsFor(user));
      } else if (req.params.action && req.params.action == "populate") {
        let user2 = Object.assign({}, user);
        user2.pets = findPetsFor(user);
        res.json(user2);
      } else {
        res.json(user);
      }
    } else if (req.params.id == "populate") {
      let arr = havePet ? findUsersWith(havePet) : db.users;
      let arr2 = [];
      for (let i = 0; i < arr.length; i++) {
        let userClone = Object.assign({}, arr[i]);
        userClone.pets = findPetsFor(userClone);
        arr2[i] = userClone;
      }
      res.json(arr2);
    } else {
      res.status(404).send("Not Found");
    }

    return;
  }

  let arr = havePet ? findUsersWith(havePet) : db.users;
  res.json(arr);
});

router.get("/pets(/:id)?(/:action)?", async (req, res) => {
  let type = req.query.type;
  let age_gt = parseInt(req.query.age_gt);
  let age_lt = parseInt(req.query.age_lt);

  if (req.params.id) {
    let pet = findPet(req.params.id);

    if (pet) {
      if (req.params.action && req.params.action == "populate") {
        let pet2 = Object.assign({}, pet);
        pet2.user = findUser(pet.userId);
        res.json(pet2);
      } else {
        res.json(pet);
      }
    } else if (req.params.id == "populate") {
      let arr = findPetsBy(type, age_gt, age_lt);
      let arr2 = [];
      for (let i = 0; i < arr.length; i++) {
        let petClone = Object.assign({}, arr[i]);
        petClone.user = findUser(petClone.userId);
        arr2[i] = petClone;
      }
      res.json(arr2);
    } else {
      res.status(404).send("Not Found");
    }

    return;
  }

  let arr = findPetsBy(type, age_gt, age_lt);
  res.json(arr);
});

function findUser(id) {
  for (let i = 0; i < db.users.length; i++) {
    if (db.users[i].id == id || db.users[i].username == id) {
      return db.users[i];
    }
  }

  return null;
}

function findUsersWith(havePet) {
    let hash = {};
    let arr = [];

    for (let i = 0; i < db.pets.length; i++) {
      if (checkType(db.pets[i], havePet)) {
        let userId = db.pets[i].userId;

        if (!hash.hasOwnProperty(userId)) {
          for (let j = 0; j < db.users.length; j++) {
            if (db.users[j].id == userId) {
              let user = db.users[j];
              hash[userId] = true;
              arr.push(user);
              break;
            }
          }
        }

      }
    }

    arr = arr.sort((a, b) => {
      return parseInt(a.id) > parseInt(b.id);
    });

    return arr;
}

function findPet(id) {
  for (let i = 0; i < db.pets.length; i++) {
    if (db.pets[i].id == id) {
      return db.pets[i];
    }
  }

  return null;
}

function findPetsBy(type, age_gt, age_lt) {
  if (type || !isNaN(age_gt) || !isNaN(age_lt)) {
    let arr = [];
    for (let i = 0; i < db.pets.length; i++) {
      if (checkType(db.pets[i], type) && checkAge(db.pets[i], age_gt, age_lt)) {
        arr.push(db.pets[i]);
      }
    }

    return arr;
  } else {
    return db.pets;
  }
}

function findPetsFor(user) {
  let res = [];

  for (let i = 0; i < db.pets.length; i++) {
    if (db.pets[i].userId == user.id) {
      res.push(db.pets[i]);
    }
  }

  return res;
}

function checkType(pet, type) {
  return !type || pet.type == type;
}

function checkAge(pet, age_gt, age_lt) {
  console.log(age_gt, age_lt);
  if (!isNaN(age_gt) && !isNaN(age_lt)) {
    return pet.age > age_gt && pet.age < age_lt;
  }
  if (!isNaN(age_gt)) {
    return pet.age > age_gt;
  }
  if (!isNaN(age_lt)) {
    return pet.age < age_lt;
  }

  return true;
}


// EXPORT
module.exports = router;
