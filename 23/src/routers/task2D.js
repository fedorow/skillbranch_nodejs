import express from 'express';


// APP
let router = express.Router();

router.get("/", async (req, res) => {
	var color = req.query.color;
	var result;

	if (color) {
		color = unescape(color.trim());

		let execHSL = /^\s*hsl\s*\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*%\s*,\s*(\d{1,3})\s*%\s*\)\s*$/ig.exec(color);
		let execRGB = /^\s*rgb\s*\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)\s*$/ig.exec(color);
		let execHEX = /^(?:0x|x|#|(?:%[0-9]{2}))?([0-9a-f]{3}(?:[0-9a-f]{3})?$)/ig.exec(color);

		if (execHSL && execHSL[1] && execHSL[2] && execHSL[3]) {
			let h = parseInt(execHSL[1]);
			let s = parseInt(execHSL[2]);
			let l = parseInt(execHSL[3]);

			if (isInLimit(h, 0, 255) && isInLimit(s, 0, 100) && isInLimit(l, 0, 100)) {
				let rgb = HSL2RGB(h, s, l);
				let r = fixLength(rgb[0].toString(16));
				let g = fixLength(rgb[1].toString(16));
				let b = fixLength(rgb[2].toString(16));
				if (r.length == 2 && g.length == 2 && b.length == 2) {
					result = "#" + r + g + b;
				}
			}
		} else if (execRGB && execRGB[1] && execRGB[2] && execRGB[3]) {
			let r = fixLength(parseInt(execRGB[1]).toString(16));
			let g = fixLength(parseInt(execRGB[2]).toString(16));
			let b = fixLength(parseInt(execRGB[3]).toString(16));
			if (r.length == 2 && g.length == 2 && b.length == 2) {
				result = "#" + r + g + b;
			}
		} else if (execHEX && execHEX[1]) {
			color = execHEX[1];
			if (color.length == 3) {
				let arr = color.split("");
				for (let i = 0; i < arr.length; i++) {
					arr[i] = fixLength(arr[i]);
				}
				result = "#" + arr.join("");
			} else {
				result = "#" + color;
			}
		}
	}

	if (result) {
		result = result.toLowerCase();
	} else {
		result = "Invalid color";
	}

	res.send(result);
});

function isInLimit(v, min, max) {
	return v >= min && v <= max;
}

function fixLength(v) {
	return v.length == 1 ? v + v : v;
}

function HSL2RGB(h, s, l) {
	h /= 360;
	s /= 100;
	l /= 100;

	var r, g, b;

	if (s == 0) {
		r = g = b = l;
	} else {
		var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
		var p = 2 * l - q;
		r = Hue2RGB(p, q, h + 1/3);
		g = Hue2RGB(p, q, h);
		b = Hue2RGB(p, q, h - 1/3);
	}
	return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
}

function Hue2RGB(p, q, t) {
	if (t < 0) {
		t += 1;
	}

	if (t > 1) {
		t -= 1;
	}

	if (t < 1/6) {
		return p + (q - p) * 6 * t;
	}

	if (t < 1/2) {
		return q;
	}

	if (t < 2/3) {
		return p + (q - p) * (2/3 - t) * 6;
	}

	return p;
}


// EXPORT
module.exports = router;

