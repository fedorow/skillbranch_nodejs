import express from 'express';


// APP
let router = express.Router();

router.get("/", async (req, res) => {
	var a = parseInt(req.query.a);
	var b = parseInt(req.query.b);

	var sum = 0;
	if (!isNaN(a)) {
		sum += a;
	}
	if (!isNaN(b)) {
		sum += b;
	}

	res.send(sum.toString());
});



// EXPORT
module.exports = router;

