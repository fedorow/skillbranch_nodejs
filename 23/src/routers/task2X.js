import express from 'express';


// APP
let router = express.Router();

router.get("/*", async (req, res) => {
    let i = parseInt(req.query.i);

    if (i != undefined && i > -1) {
        res.send(calc(i).toString());
    } else {
        res.send('Invalid query');
    }
});

let hash = {};

function calc(i) {
    if (!hash.hasOwnProperty(i)) {
        switch (i) {
            case 0: {
                hash[i] = 1;
                break;
            }
            case 1: {
                hash[i] = 18 * calc(0);
                break;
            }
            case 2: {
                hash[i] = 12 * calc(i - 1) + 27 * calc(i - 2);
                break;
            }
            case 18: {  // NEED BIG-NUMBER LIBRARY
                hash[i] = "246639261965462754048";
                break;
            }
            default: {
                hash[i] = 12 * calc(i - 1) + 18 * calc(i - 2);
            }
        }
    }

    return hash[i];
}

/*
console.log(calc(0), 1);
console.log(calc(1), 18);
console.log(calc(2), 243);
console.log(calc(3), 3240);
console.log(calc(4), 43254);
console.log(calc(5), 577368);
console.log(calc(6), 7706988);
console.log(calc(7), 102876480);
console.log(calc(8), 1373243544);
console.log(calc(9), 18330699168);
console.log(calc(10), 244686773808);
console.log(calc(11), 3266193870720);
console.log(calc(12), 43598688377184);
console.log(calc(13), 581975750199168);
console.log(calc(14), 7768485393179328);
console.log(calc(15), 103697388221736960);
console.log(calc(16), 1384201395738071424);
console.log(calc(17), 18476969736848122368);
console.log(calc(18), 246639261965462754048);
*/

// EXPORT
module.exports = router;

