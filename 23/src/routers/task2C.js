import express from 'express';


// APP
let router = express.Router();

router.get("/", async (req, res) => {
	var username = req.query.username;
	var result;

	if (username) {
		username = /(?:(?:http[s]?:)?(?:\/\/)?[a-z0-9._\-]+\/)?@?([a-z0-9._\-]+)(?:(?:\/|\?){1}[a-z0-9._\-=]+)?/g.exec(username)[1];
		result = "@" + username.trim();
	} else {
		result = "Invalid username";
	}

	res.send(result);
});


// EXPORT
module.exports = router;

